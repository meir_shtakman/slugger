export default function slugger(...args){
    return args.reduce((megaString,str)=>{
        return `${megaString?megaString+"-":""}${str.split(' ').map(s=>s.trim()).join('-').trim()}`
    },"");
}